FROM node:alpine

ARG VCS_REF

RUN npm install -g redoc-cli

VOLUME /project

WORKDIR /project

ENTRYPOINT ["redoc-cli"]